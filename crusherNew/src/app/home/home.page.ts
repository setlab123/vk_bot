import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { Storage } from '@ionic/storage';

import { NgModule } from '@angular/core';

import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";




import * as $ from 'jquery';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: "",
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage]
})


export class HomePage {


  ngOnInit() {
    this.buttonUpdate();
  }

  // status_data: boolean;
  status_data = false;
  // constructor() {}
  // all_data: Array<any> = [];

  crusher_data: Array<any> = []; // данные по дробилке

  constructor(private http: HttpClient) { }
  // constructor(private http: HttpClient, private storage: Storage) { }





  presentAlert() {
    const alert = document.createElement('ion-alert');
    // alert.header = 'Alert';
    alert.subHeader = 'Ошибка';
    alert.message = 'Проверьте интернет-соединение.';
    alert.buttons = ['OK'];
  
    document.body.appendChild(alert);
    return alert.present();
  }

  gap_array = [];
  hamer_array = [];
  conveyor1_array = [];
  conveyor2_array = [];




  buttonUpdate() {
    this.gap_array = [];
    this.hamer_array = [];
    this.conveyor1_array = [];
    this.conveyor2_array = [];

    console.log('Click');
    this.apdateData();

    let data_from_bd = this.apdateData();

    
    if (data_from_bd == "false") {
      this.status_data = false;
      this.presentAlert();
    } else {
      this.status_data = true;
    }

    let json_data_from_bd = JSON.parse(data_from_bd);
    this.crusher_data = json_data_from_bd.crusher[0];

    let gap = json_data_from_bd.crusher[0].gap;
    let count_gap = Object.keys(gap);

    for (let i = 0; i < count_gap.length; i++) {
      this.gap_array.push(json_data_from_bd.crusher[0].gap[count_gap[i]]);
    }
    console.log(this.gap_array);

    let hamer = json_data_from_bd.crusher[0].hamer;
    this.hamer_array.push(hamer);
    console.log(this.hamer_array);

    let conveyor1 = json_data_from_bd.crusher[0].conveyor1;
    this.conveyor1_array.push(conveyor1);
    console.log(this.conveyor1_array);

    let conveyor2 = json_data_from_bd.crusher[0].conveyor2;
    this.conveyor2_array.push(conveyor2);
    console.log(this.conveyor2_array);

    // this.status_data = true;
  }

  apdateData() {
    // this.all_data = [];
    var result = "";
    $.ajax({
      type: 'POST',
      url: "http://188.225.39.107/CRUSHER/",
      dataType: 'text',
      data: { 'action': "select" },
      async: false,
      success: function (data) {
        data = data.replace(/['"]+/g, '"');
        console.log(data);
        result = data;
        // this.status_data = true;
      },
      error: function () {
        // alert('Error occured');
        console.log('Error occured');
        result = "false";
        // this.status_data = false;
      }
    });
    return result;
  }



}



