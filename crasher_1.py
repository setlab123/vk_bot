# -*- coding: utf-8 -*-
import vk_api
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
from vk_api.utils import get_random_id
import json
import random

# your_group_token = "55dfe00e75f1f578ae925c160e3740e451eb6a3578e17e6cfe6ef87bb3deff615fbc5fa8bdc42697cb66c"
your_group_token = "40426646524ac9c0a075fc54430729b7c4a927dd81a23f1440b3cfc64e67ed48f8de489223bad4d4540da"


# your_group_id = 193216117
your_group_id = 193218173

vk_session = vk_api.VkApi(token=your_group_token)
longpoll = VkBotLongPoll(vk_session, your_group_id)
vk = vk_session.get_api()


def main():
    i = 0
    while(1):
        peer_id = 2000000005
        group_messages = vk.messages.getHistory(peer_id=peer_id)
        last_message = group_messages.get(u'items')[0].get(u'text')
        print(last_message)
        text = last_message

        data = {
                    "val_current_relay_1" : 0,
                    "val_sensor_speed_1" : 1,
                    "overheating_motor_1" : 0,
                    "val_clamping_on_1" : 0,
                    "val_speed_1" : random.randint(1,22),
                    "val_quantity_revers_1" : 4,

                    "val_current_relay_2" : 1,
                    "val_sensor_speed_2" : 0,
                    "overheating_motor_2" : 0,
                    "val_clamping_on_2" : 1,
                    "val_speed_2" : random.randint(1,44),
                    "val_quantity_revers_2" : 3,

                    "hammer_on_off" : 1,    
                    "hammer_crash_overload" : 0,
                    "hammer_speed" : 3,
                    "hammer_conveyor_off" : 0,


                    # // транспортер выкидной
                    "conveyor_on_off_1" : 1,
                    "conveyor_current_relay_1" : 0,

                    # //  транспортер подающий
                    "conveyor_on_off_2" : 1,
                    "conveyor_current_relay_2" : 1,
        }
        print(data)
        out = json.dumps(data)
        print(out)
        message = out
        # vk.messages.send(peer_id=peer_id, message=message, random_id = get_random_id())


        if(text=="Crasher 1"):

            # message = "Crasher №1 на месте"
            message = out
            vk.messages.send(peer_id=peer_id, message=message, random_id = get_random_id())
        i = i+1
        print(i)



if __name__ == '__main__':
    main()
