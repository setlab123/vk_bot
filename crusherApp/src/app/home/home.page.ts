import { Component, ViewChild, OnInit, Input,  AfterViewInit, ViewChildren} from '@angular/core';
import {NgModule} from '@angular/core';

import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

// import { HomePage } from "./home.page";
// import { ExpandableComponent } from "../components/expandable/expandable.component";

// import { MatExpansionModule } from '@angular/material/expansion';



import * as $ from 'jquery';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: "",
        component: HomePage
      }
    ]),
    // MatExpansionModule
  ],
  declarations: [HomePage]
})


export class HomePage {



sleep(millis) {
    var t = (new Date()).getTime();
    var i = 0;
    while (((new Date()).getTime() - t) < millis) {
        i++;
    }
}

getURL(metod,params){
  params = params || {};
  params['access_token'] = 'af48a917d3d405faf6796c04f1b811c8c2ec48d51b5e0060b666d8dc8d213077df9955d2bd5257ab15779'
  params['v'] = '5.103'
  return 'https://api.vk.com/method/' + metod + '?' + $.param(params);
}

getURL2(metod,params){
  params = params || {};
  params['access_token'] = '40426646524ac9c0a075fc54430729b7c4a927dd81a23f1440b3cfc64e67ed48f8de489223bad4d4540da'
  // params['access_token'] = '55dfe00e75f1f578ae925c160e3740e451eb6a3578e17e6cfe6ef87bb3deff615fbc5fa8bdc42697cb66c'
  params['v'] = '5.103'
  return 'https://api.vk.com/method/' + metod + '?' + $.param(params);
}

postText(){
  let Today = new Date(); 
  let time = Today.getTime();
  let massage = time + '\n'+ 'Crasher 1';
  // let massage = 'Crasher 1';
  $.ajax({
      url: this.getURL('wall.post', {'owner_id': '-193218173', 'from_group': '1', 'message': massage}),
      method: 'GET',
      dataType: 'JSONP'
  })
}

getText(){
$.ajax({
    url: this.getURL('wall.get', {'owner_id': '-193218173', 'count': '1'}),
    method: 'GET',
    dataType: 'JSONP',
    success: function (data){
        console.log(data.response.items[0].text);
        let text_post = data.response.items[0].text
        document.getElementById("text_post").innerHTML = text_post;
    }
})
}

sendMessage(){
  $.ajax({

      url: this.getURL2('messages.send', {'peer_id': 2000000005, 'message': 'Crasher 1', 'random_id': 0}),
      method: 'GET',
      dataType: 'JSONP'
  })
}

getMessage(){
  $.ajax({
      // url: getURL2('messages.getConversationMembers', {'peer_id': 2000000001}),
      url: this.getURL2('messages.getHistory', {'peer_id': 2000000005}),
      method: 'GET',
      dataType: 'JSONP',
      success: function (data){
          console.log(data);
          var text_msg = data.response.items[0].text
          console.log(text_msg);
          // document.getElementById("text_msg").innerHTML = text_msg;
          data = JSON.parse(text_msg)

          var val_current_relay_1 = data.val_current_relay_1
          var val_sensor_speed_1 = data.val_sensor_speed_1
          var overheating_motor_1 = data.overheating_motor_1
          var val_clamping_on_1 = data.val_clamping_on_1
          var val_speed_1 = data.val_speed_1
          var val_quantity_revers_1 = data.val_quantity_revers_1

          var val_current_relay_2 = data.val_current_relay_2
          var val_sensor_speed_2 = data.val_sensor_speed_2
          var overheating_motor_2 = data.overheating_motor_2
          var val_clamping_on_2 = data.val_clamping_on_2
          var val_speed_2 = data.val_speed_2
          var val_quantity_revers_2 = data.val_quantity_revers_2

          var hammer_on_off = data.hammer_on_off
          var hammer_crash_overload = data.hammer_crash_overload
          var hammer_speed = data.hammer_speed
          var hammer_conveyor_off = data.hammer_conveyor_off

          
          // транспортер выкидной
          var conveyor_on_off_1 = data.conveyor_on_off_1
          var conveyor_current_relay_1 = data.conveyor_current_relay_1
  
          //  транспортер подающий
          var conveyor_on_off_2 = data.conveyor_on_off_2
          var conveyor_current_relay_2 = data.conveyor_current_relay_2



          var test_array=[
              
          {
              'name': 'val_current_relay_1',
              'val':val_current_relay_1
          },
          {
              'name': 'val_sensor_speed_1',
              'val':val_sensor_speed_1
          },
          {
              'name': 'overheating_motor_1',
              'val':overheating_motor_1
          },
          {
              'name': 'val_clamping_on_1',
              'val':val_clamping_on_1
          },


          {
              'name': 'val_current_relay_2',
              'val':val_current_relay_2
          },
          {
              'name': 'val_sensor_speed_2',
              'val':val_sensor_speed_2
          },
          {
              'name': 'overheating_motor_2',
              'val':overheating_motor_2
          },
          {
              'name': 'val_clamping_on_2',
              'val':val_clamping_on_2
          },

          ]

          for(let i=0; i<8; i++){
              console.log(test_array[i].val)

              if (test_array[i].val=='1') {
                  $("#color_"+test_array[i].name).css('background-color', 'red');
              } else if(test_array[i].val=='0') {
                  $("#color_"+test_array[i].name).css('background-color', 'green');
                  console.log(test_array[i].name)
              } else {
                  $("#color_"+test_array[i].name) .css('background-color', '#92949C');
              }
          }

          // document.getElementById("val_current_relay_1").innerHTML = val_current_relay_1;
          // document.getElementById("val_sensor_speed_1").innerHTML = val_sensor_speed_1;
          // document.getElementById("overheating_motor_1").innerHTML = overheating_motor_1;
          // document.getElementById("val_clamping_on_1").innerHTML = val_clamping_on_1;
          // document.getElementById("val_speed_1").innerHTML = val_speed_1;
          // document.getElementById("val_quantity_revers_1").innerHTML = val_quantity_revers_1;



          $('#val_current_relay_1').text(val_current_relay_1);
          $('#val_sensor_speed_1').text(val_sensor_speed_1);
          $('#overheating_motor_1').text(overheating_motor_1);
          $('#val_clamping_on_1').text(val_clamping_on_1);
          $('#val_speed_1').text(val_speed_1);
          $('#val_quantity_revers_1').text(val_quantity_revers_1);

          $('#val_current_relay_2').text(val_current_relay_2);
          $('#val_sensor_speed_2').text(val_sensor_speed_2);
          $('#overheating_motor_2').text(overheating_motor_2);
          $('#val_clamping_on_2').text(val_clamping_on_2);
          $('#val_speed_2').text(val_speed_2);
          $('#val_quantity_revers_2').text(val_quantity_revers_2);


          $('#hammer_on_off').text(hammer_on_off);
          $('#hammer_crash_overload').text(hammer_crash_overload);
          $('#hammer_speed').text(hammer_speed);
          $('#hammer_conveyor_off').text(hammer_conveyor_off);

          $('#conveyor_on_off_1').text(conveyor_on_off_1);
          $('#conveyor_current_relay_1').text(conveyor_current_relay_1);

          $('#conveyor_on_off_2').text(conveyor_on_off_2);
          $('#conveyor_current_relay_2').text(conveyor_current_relay_2);
          
      }
  })
}


apdateData(){
  this.sendMessage();
  this.sleep(1000);
  this.getMessage();
}


buttonUpdate() {
  // alert();  
  console.log('Click');
  // this.sendMessage();
  this.apdateData();
}


}
