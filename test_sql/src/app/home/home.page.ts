import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

import { NgModule } from '@angular/core';

import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";




import * as $ from 'jquery';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: "",
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage]
})


export class HomePage {

  // constructor() {}
  constructor(private http: HttpClient, private storage: Storage) { }


  getData() {

    let url = 'http://localhost:8081';
    this.http.get(`${url}`).subscribe(
      data => {
        console.log(data);
        var text_msg = data[0]


        console.log(text_msg);
        // document.getElementById("text_msg").innerHTML = text_msg;
        text_msg = text_msg.crusher_data;
        text_msg = JSON.parse(text_msg)


        var val_current_relay_1 = text_msg.val_current_relay_1
        var val_sensor_speed_1 = text_msg.val_sensor_speed_1
        var overheating_motor_1 = text_msg.overheating_motor_1
        var val_clamping_on_1 = text_msg.val_clamping_on_1
        var val_speed_1 = text_msg.val_speed_1
        var val_quantity_revers_1 = text_msg.val_quantity_revers_1

        var val_current_relay_2 = text_msg.val_current_relay_2
        var val_sensor_speed_2 = text_msg.val_sensor_speed_2
        var overheating_motor_2 = text_msg.overheating_motor_2
        var val_clamping_on_2 = text_msg.val_clamping_on_2
        var val_speed_2 = text_msg.val_speed_2
        var val_quantity_revers_2 = text_msg.val_quantity_revers_2

        var hammer_on_off = text_msg.hammer_on_off
        var hammer_crash_overload = text_msg.hammer_crash_overload
        var hammer_speed = text_msg.hammer_speed
        var hammer_conveyor_off = text_msg.hammer_conveyor_off


        // транспортер выкидной
        var conveyor_on_off_1 = text_msg.conveyor_on_off_1
        var conveyor_current_relay_1 = text_msg.conveyor_current_relay_1

        //  транспортер подающий
        var conveyor_on_off_2 = text_msg.conveyor_on_off_2
        var conveyor_current_relay_2 = text_msg.conveyor_current_relay_2



        var test_array = [

          {
            'name': 'val_current_relay_1',
            'val': val_current_relay_1
          },
          {
            'name': 'val_sensor_speed_1',
            'val': val_sensor_speed_1
          },
          {
            'name': 'overheating_motor_1',
            'val': overheating_motor_1
          },
          {
            'name': 'val_clamping_on_1',
            'val': val_clamping_on_1
          },


          {
            'name': 'val_current_relay_2',
            'val': val_current_relay_2
          },
          {
            'name': 'val_sensor_speed_2',
            'val': val_sensor_speed_2
          },
          {
            'name': 'overheating_motor_2',
            'val': overheating_motor_2
          },
          {
            'name': 'val_clamping_on_2',
            'val': val_clamping_on_2
          },

        ]

        for (let i = 0; i < 8; i++) {
          console.log(test_array[i].val)

          if (test_array[i].val == '1') {
            $("#color_" + test_array[i].name).css('background-color', 'red');
          } else if (test_array[i].val == '0') {
            $("#color_" + test_array[i].name).css('background-color', 'green');
            console.log(test_array[i].name)
          } else {
            $("#color_" + test_array[i].name).css('background-color', '#92949C');
          }
        }

        // document.getElementById("val_current_relay_1").innerHTML = val_current_relay_1;
        // document.getElementById("val_sensor_speed_1").innerHTML = val_sensor_speed_1;
        // document.getElementById("overheating_motor_1").innerHTML = overheating_motor_1;
        // document.getElementById("val_clamping_on_1").innerHTML = val_clamping_on_1;
        // document.getElementById("val_speed_1").innerHTML = val_speed_1;
        // document.getElementById("val_quantity_revers_1").innerHTML = val_quantity_revers_1;



        $('#val_current_relay_1').text(val_current_relay_1);
        $('#val_sensor_speed_1').text(val_sensor_speed_1);
        $('#overheating_motor_1').text(overheating_motor_1);
        $('#val_clamping_on_1').text(val_clamping_on_1);
        $('#val_speed_1').text(val_speed_1);
        $('#val_quantity_revers_1').text(val_quantity_revers_1);

        $('#val_current_relay_2').text(val_current_relay_2);
        $('#val_sensor_speed_2').text(val_sensor_speed_2);
        $('#overheating_motor_2').text(overheating_motor_2);
        $('#val_clamping_on_2').text(val_clamping_on_2);
        $('#val_speed_2').text(val_speed_2);
        $('#val_quantity_revers_2').text(val_quantity_revers_2);


        $('#hammer_on_off').text(hammer_on_off);
        $('#hammer_crash_overload').text(hammer_crash_overload);
        $('#hammer_speed').text(hammer_speed);
        $('#hammer_conveyor_off').text(hammer_conveyor_off);

        $('#conveyor_on_off_1').text(conveyor_on_off_1);
        $('#conveyor_current_relay_1').text(conveyor_current_relay_1);

        $('#conveyor_on_off_2').text(conveyor_on_off_2);
        $('#conveyor_current_relay_2').text(conveyor_current_relay_2);
      }
    );


  }



  apdateData() {
    this.getData();
  }


  buttonUpdate() {
    // alert();  
    console.log('Click');
    this.apdateData();
  }




  test_but() {
    console.log('Click');
  }


}



